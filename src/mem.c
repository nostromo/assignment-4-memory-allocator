#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  // the size of the block with query capacity
  size_t query_size = size_from_capacity((block_capacity) {.bytes=query}).bytes;
  size_t region_size = region_actual_size(query_size);

  // try to allocate a region at the specified address
  void* region = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

  // if it fails, then allocate it anywhere
  if (region == MAP_FAILED) region = map_pages(addr, region_size, 0);
  if (region == MAP_FAILED) return REGION_INVALID;

  block_init(region, (block_size){.bytes = region_size}, NULL);

  return (struct region){
    .addr = region,
    .size = region_size,
    .extends = (region == addr)
  };
}

static void* block_after( struct block_header const* block )         ;
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    // Iterating over regions, call munmap for each
  struct block_header* head = HEAP_START; // the first block of the region
  struct block_header* tail; // the last block of the region
  while (head) {
    tail = head;
    // move the tail away while possible
    while (tail->next && blocks_continuous(tail, tail->next)) tail = tail->next;
    struct block_header *next = tail->next; // link to the next region

    // munmap region
    // |___header1____|____data1___|  ...  |___header2____|____data2___|
    // |_____________ptr2-ptr1_____________|____________size___________|
    munmap(head, tail->contents - head->contents + size_from_capacity(tail->capacity).bytes);
    head = next;
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block || !block_splittable(block, query)) return false;

  void* rest = block->contents + query;
  block_size rest_size = {.bytes = block->capacity.bytes - query};
  block_init(rest, rest_size, block->next);

  block->next = rest;
  block->capacity.bytes = query;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next = block->next;
  if (!next || !mergeable(block, next)) return false;

  block->capacity.bytes += size_from_capacity(next->capacity).bytes;
  block->next = next->next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  for (; block; block = block->next) {
    while (try_merge_with_next(block));
    if (block->is_free && block_is_big_enough(sz,block))
      return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
    if (!block->next) return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
  }
  return (struct block_search_result) {.type=BSR_CORRUPTED, .block = NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct region expansion = alloc_region(block_after(last), query);
  if (region_is_invalid(&expansion)) return NULL;
  last->next = expansion.addr;

  // if new region extends previous one, try merging blocks
  if (expansion.extends && try_merge_with_next(last)) return last;
  return expansion.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (query <= 0 || !heap_start) return NULL;
  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if (result.type != BSR_REACHED_END_NOT_FOUND) return result.block;

  // If no good block was found till the end, allocate another region
  struct block_header* extended = grow_heap(result.block, query);
  return try_memalloc_existing(query, extended).block;
}

void* _malloc( size_t query ) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
