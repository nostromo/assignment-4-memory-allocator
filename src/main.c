#include "mem_internals.h"
#include "mem.h"
#include <assert.h>

static struct block_header* debug_block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void test_malloc() {
    printf("Test malloc\n");
    void* heap = heap_init(0);
    assert(heap != NULL);
    debug_heap(stdout, HEAP_START);

    void* block = _malloc(23);
    assert(block != NULL);
    debug_heap(stdout, HEAP_START);

    struct block_header* header = debug_block_get_header(block);
    assert(!header->is_free);

    _free(block);
    heap_term();

    printf("\n");
}

void test_free_1() {
    printf("Test free single block\n");
    void* heap = heap_init(0);
    assert(heap != NULL);

    void* block1 = _malloc(240);
    void* block2 = _malloc(480);
    void* block3 = _malloc(720);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);
    debug_heap(stdout, HEAP_START);

    _free(block2);
    debug_heap(stdout, HEAP_START);

    struct block_header* header1 = debug_block_get_header(block1);
    struct block_header* header2 = debug_block_get_header(block2);
    assert(!header1->is_free);
    assert(header2->is_free);
    assert(header1->next == header2);

    _free(block1);
    heap_term();
    printf("\n");
}

void test_free_2() {
    printf("Test free two blocks\n");
    void* heap = heap_init(0);
    assert(heap != NULL);

    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    void* block4 = _malloc(100);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);
    assert(block4 != NULL);
    debug_heap(stdout, HEAP_START);

    _free(block3);
    _free(block2);
    debug_heap(stdout, HEAP_START);

    struct block_header* header2 = debug_block_get_header(block2);
    struct block_header* header4 = debug_block_get_header(block4);
    assert(header2->next == header4);

    _free(block1);
    _free(block4);
    heap_term();
    printf("\n");
}

void test_grow_extends() {
    printf("Test grown region extends\n");
    void* heap = heap_init(0);
    assert(heap != NULL);
    debug_heap(stdout, HEAP_START);

    void* block = _malloc(REGION_MIN_SIZE);
    assert(block != NULL);
    debug_heap(stdout, HEAP_START);

    struct block_header* header = debug_block_get_header(block);
    assert(header->capacity.bytes == REGION_MIN_SIZE);

    _free(block);
    debug_heap(stdout, HEAP_START);

    heap_term();
    printf("\n");
}

void test_grow_random() {
    printf("Test grown region doesn't extend\n");
    void* heap = heap_init(0);
    assert(heap != NULL);
    debug_heap(stdout, HEAP_START);

    void* barrier = mmap(HEAP_START + REGION_MIN_SIZE, 240, 0, MAP_PRIVATE | 0x20, -1, 0);
    assert(barrier);

    void* block = _malloc(REGION_MIN_SIZE);
    assert(block != NULL);
    debug_heap(stdout, HEAP_START);

    struct block_header* header = debug_block_get_header(block);
    assert(header->capacity.bytes == REGION_MIN_SIZE);
    assert(header != HEAP_START);

    _free(block);
    debug_heap(stdout, HEAP_START);

    munmap(barrier, 240);
    heap_term();
    printf("\n");
}

int main() {
    test_malloc();
    test_free_1();
    test_free_2();
    test_grow_extends();
    test_grow_random();
    return 0;
}

